/**
 * Created by tharindu on 2/14/2018.
 */
var mysqlDb = require('../../models/mysql');
/**
 * generate tables in form based relations
 * @param modelName
 * @param property
 * @returns {string}
 */
function tableBodyBox(modelName ,fields){
    var optionsHTML = '',results;
    var i= 0;

    mysqlDb.getModel(modelName).findAll().then(function(arg){
        results = arg;
    });


    while(results === undefined && i<1000000000 ) {
        require('deasync').runLoopOnce();
        i++;
    }


    results.forEach(function(result){
        console.log(result);
        var data = JSON.parse(JSON.stringify(result.dataValues));

        optionsHTML +='<tr>';
       for(var key in data){
           var bool = false;
         var field=   fields.toString().replace('[','');
              field.replace(']','');
           var array = field.split(',');
           for(var i=0 ; i<array.length;i++){
               if(array[i]===key){
                   bool=true;
                   break;
               }
           }

           if(bool){
               optionsHTML+='<th>'+data[key]+'</th>';
           }

       }
       optionsHTML += '</tr>'

    });

    return optionsHTML;
}


module.exports = tableBodyBox;
