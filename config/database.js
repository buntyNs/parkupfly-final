var config = require('../config.json');

var mysql = require('mysql');

module.exports = mysql.createConnection({
    host: "http://localhost",
    user: config.mysql.username,
    password: config.mysql.password,
    database: config.mysql.database,
});